# semantic-search-challenge
This repository is to share code developed to answer the amplyfi interview technical challenge.

At present, the project establishes a distributed (docker-based) data processing pipeline for JSON files. The main toolchain is built on Elasticsearch and Kibana, with a custom Python service handling JSON ingestion, word embedding derivation and cover tree based semantic search functionality. Much of the interaction between the services in the pipeline is RESTful.

Prerequisites:
* docker CE
* docker-compose

Your first step should be to build and start the docker services using:
```
docker-compose -f ek_stack_compose.yml build
```
```
docker-compose -f ek_stack_compose.yml up -d
```

By default, Kibana runs on localhost:5601 and the Python ingest service runs on localhost:8001. Try uploading a large archive of JSON files to the ingest server and tracking the progress of the ingest service by watching the data roll into Kibana.

The author certifies that this work is both
* terrible and
* of little use for anything practical.

Please take a look at the issues list for my estimation of what important problems need to be solved to make this project a useful tool.

Major credit is due to Manzil Zaheer, who open sourced the below cover tree algorithm with Python bindings and to Huang, Socher, and Manning for their 2012 submission to the ACL and their publicly available (not to mention lightweight!) word embeddings dataset.
https://github.com/manzilzaheer/CoverTree
http://www.socher.org/index.php/Main/ImprovingWordRepresentationsViaGlobalContextAndMultipleWordPrototypes
