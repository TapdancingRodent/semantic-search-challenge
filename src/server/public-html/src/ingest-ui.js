function onLoad() {}
function onResize() {}

function uploadIngest() {
	console.log("Uploading file for ingest...")
	var files = document.getElementById("ingestfile").files
	fileData = new FormData();
	fileData.append('file', files[0])
	$.ajax({
		url: window.location.origin + '/ingest',
		data: fileData,
		cache: false,
		contentType: false,
		processData: false,
		enctype: 'multipart/form-data',
		type: 'POST',
		success: function(data) {
			console.log('Success!');
		},
	});
}

function uploadSearch() {
	console.log("Uploading file for search...")
	var files = document.getElementById("searchfile").files
	fileData = new FormData();
	fileData.append('file', files[0])
	$.ajax({
		url: window.location.origin + '/search',
		data: fileData,
		cache: false,
		contentType: false,
		processData: false,
		enctype: 'multipart/form-data',
		type: 'POST',
		success: function(data) {
			console.log('Success!');
			document.getElementById('resultstext').innerHTML = data;
		},
	});
}
