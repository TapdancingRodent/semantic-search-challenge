#!/usr/bin/python
import argparse
from collections import defaultdict
from covertree import CoverTree
from elasticsearch import Elasticsearch
from itertools import cycle
import logging
import numpy as np
import os
import simplejson as json
import sys

sys.settrace
LOGGER = logging.getLogger("ek.loader")

# Validation function for folder inputs parsed by argparse
class ValidateFolder(argparse.Action):
	def __call__(self, parser, namespace, values, option_string=None):
		if not os.path.isdir(os.path.abspath(values)):
			raise ValueError("Specificed data dir is not a valid directory")
		setattr(namespace, self.dest, os.path.abspath(values))

# Open a connection the an elasticsearch server
def es_connect(es_host, es_port, es_index):
	LOGGER.debug("Opening connection to elasticearch server through the HTTP API")
        es_client = Elasticsearch([{"host":es_host, "port":es_port}])
        es_client.indices.create(index=es_index, ignore=400)
	return es_client

# Load word embeddings for generating document embeddings
def embeddings_load(embedding_dir):
	LOGGER.debug("Loading word embedding dataset")
        embed_vecs = {}
        with open(os.path.join(embedding_dir, "vocab.txt"), 'r') as word_file:
                with open(os.path.join(embedding_dir, "wordVectors.txt"), 'r') as vector_file:
                        for word in word_file:
                                vector_str = vector_file.readline().strip(" \n")
                                embed_vecs[word.strip('\n')] = np.array(vector_str.split(' '), dtype=float)
	return embed_vecs

# Parse a json file at a given path, returning a simplejson object or None
def parse_json_file(json_file):
	json_parsed = None
	try:
		json_parsed = json.load(json_file)
	except Exception as ex:
		LOGGER.warning("Failed to parse json file: {}".format(file_path), exc_info=True)
	
	return json_parsed

# Parse an open json file, returning a simplejson object or None
def parse_json_file_by_path(file_path):
	with open(file_path, 'r') as json_file:
		return parse_json_file(json_file)

# Calculate word embeddings for an open json file
def get_embeddings(json_parsed, embed_vecs):
	vec_len = len(embed_vecs.values()[0])
	if "m_BiGrams" in json_parsed and len(json_parsed["m_BiGrams"]) > 0:
		document_embeddings = []
		document_bigrams = []
		for bigram in json_parsed["m_BiGrams"]:
			words = bigram.split(' ')
			
			if all([word in embed_vecs for word in words]):
				document_embedding = np.empty((0,))
				for word in words:
					document_embedding = np.append(document_embedding, embed_vecs[word])
		
				document_embeddings.append(document_embedding)
				document_bigrams.append(bigram)
		
		return (document_embeddings, document_bigrams)
	
	else:
		return (None, None)

# Ingest documents from a specified data directory, process into elasticsearch and generate a cover tree
def ingest(es_client, es_index, embed_vecs, data_dir):
	vec_len = len(embed_vecs.values()[0])
	
	LOGGER.debug("Parsing JSON files, writing to elasticsearch and calculating embeddings")
	embeddings = []
	bigrams = []
	document_name_to_id = {}
	document_id = 0
	for root, _, files in os.walk(data_dir):
		for file_name in files:
			json_parsed = parse_json_file_by_path(os.path.join(root, file_name))
			if json_parsed is not None:
				document_id = es_client.index(index=es_index, doc_type="txt", body=json_parsed)["_id"]
				#document_id += 1
				document_name_to_id[file_name] = document_id
				
				document_embeddings, document_bigrams = get_embeddings(json_parsed, embed_vecs)
				
				if document_embeddings is not None:
					embeddings += document_embeddings
					for d_b in document_bigrams:
						bigrams.append((file_name, document_id, d_b))
	
	LOGGER.debug("Preprocessing embeddings for cover tree generation")
	embeddings = np.array(embeddings, dtype=float)
	
	if embeddings.shape[0] < 50000: # Guard against a segfault in the CoverTree library code
		raise IOError("The number of mined embeddings ({}) was not enough to create a cover tree".format(embeddings.shape[0]))
	
	offset = embeddings.min()
	embeddings = embeddings - offset
	scale = embeddings.max()
	embeddings = embeddings / scale
	
	LOGGER.debug("Generating cover tree")
	cover_tree = CoverTree.from_matrix(embeddings)
	
	return (bigrams, embeddings, cover_tree, offset, scale)

def tree_search(bigrams, embeddings, cover_tree, search_embeddings, k):
	LOGGER.debug("Searching for semantically similar documents")
	if search_embeddings.shape[0] < 20: # Guard against a segfault originating in the CoverTree library
		s_e_padded = np.append(search_embeddings, embeddings[0:20,:], 0)
		neighbours = cover_tree.kNearestNeighbours(s_e_padded, k)
		if search_embeddings.shape[0] == 0:
			neighbours = neighbours[0,:,:]
		else:
			neighbours = neighbours[0:search_embeddings.shape[0],:,:]
	else:
		neighbours = cover_tree.kNearestNeighbours(search_embeddings, k)

        neighbour_data = []
        for n in range(len(bigrams)):
                if embeddings[n,:] in neighbours:
                        neighbour_data.append(bigrams[n])
	
        return neighbour_data

# Test recall of similar documents from the cover tree
def cover_tree_test(bigrams, embeddings, cover_tree):
	return tree_search(bigrams, embeddings, cover_tree, embeddings[10:11,:], 2)

# Only run when Python starts from the command line
if __name__ == "__main__":
	parser = argparse.ArgumentParser('Config options for JSON document ingest')
	parser.add_argument("--host", dest="es_host", default="0.0.0.0")
	parser.add_argument("--port", dest="es_port", type=int, default=9200)
	parser.add_argument("--index", dest="es_index", default="ingested")
	parser.add_argument("--data-dir", dest="data_dir", action=ValidateFolder, required=True)
	parser.add_argument("--embedding-dir", dest="embedding_dir", action=ValidateFolder, required=True)
	cl_args = parser.parse_args()	
	
	es_client = es_connect(cl_args.es_host, cl_args.es_port, cl_args.es_index)
	embed_vecs = embeddings_load(cl_args.embedding_dir)
	bigrams, embeddings, cover_tree, offset, scale = ingest(es_client, cl_args.es_index, embed_vecs, cl_args.data_dir)
	neighbour_data = cover_tree_test(bigrams, embeddings, cover_tree)

