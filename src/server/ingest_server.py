#!/usr/bin/python
import argparse
from covertree import CoverTree
from elasticsearch import Elasticsearch
import flask
import ingest_utils
import logging
import numpy as np
import os
import simplejson as json
import sys
import zipfile

sys.settrace
LOGGER = logging.getLogger("ek.loader")

app = flask.Flask("ingest_server", static_folder="/server/public-html")

@app.route("/")
@app.route("/site")
def sendBase():
	return flask.redirect(flask.url_for("sendDoc", document="index.html"), code=307)

@app.route("/site/<path:document>")
def sendDoc(document):
	return flask.send_from_directory("/server/public-html", document)

@app.route("/ingest", methods=["POST"])
def ingest():
	global BIGRAMS, EMBEDDINGS, COVER_TREE, OFFSET, SCALE
	LOGGER.debug("Received ingest file from client...")
	es_client = ingest_utils.es_connect(ES_HOST, ES_PORT, ES_INDEX)
	json_archive = zipfile.ZipFile(flask.request.files["file"])
	data_dir = os.path.abspath(app.config["UPLOAD_FOLDER"])
	json_archive.extractall(data_dir)
	BIGRAMS, EMBEDDINGS, COVER_TREE, OFFSET, SCALE = ingest_utils.ingest(es_client, ES_INDEX, EMBED_VECS, data_dir)
	return ("Files processed", 200)

@app.route("/search", methods=["POST"])
def search():
	if not BIGRAMS:
		return ("Cover tree has not been populated", 400)
	else:
		json_parsed = ingest_utils.parse_json_file(flask.request.files["file"])
		if not json_parsed:
			return ("Unable to parse the supplied JSON file", 400)
		document_embeddings, _ = ingest_utils.get_embeddings(json_parsed, EMBED_VECS)
		document_embeddings = np.array(document_embeddings, dtype=float)
		document_embeddings = document_embeddings - OFFSET
		document_embeddings = document_embeddings / SCALE
		
		neighbour_data = ingest_utils.tree_search(BIGRAMS, EMBEDDINGS, COVER_TREE, document_embeddings, 5)
		return (str(neighbour_data), 200)

# Only run when Python starts from the command line
if __name__ == "__main__":
	parser = argparse.ArgumentParser("Config options for JSON document ingest")
	parser.add_argument("--host", dest="es_host", default="0.0.0.0")
	parser.add_argument("--port", dest="es_port", type=int, default=9200)
	parser.add_argument("--index", dest="es_index", default="ingested")
	parser.add_argument("--embedding-dir", dest="embedding_dir", action=ingest_utils.ValidateFolder, required=True)
	parser.add_argument("--uploads-dir", dest="uploads_dir", action=ingest_utils.ValidateFolder, required=True)
	cl_args = parser.parse_args()	

	ES_HOST = cl_args.es_host
	ES_PORT = cl_args.es_port
	ES_INDEX = cl_args.es_index
	EMBED_VECS = ingest_utils.embeddings_load(cl_args.embedding_dir)
	app.config["UPLOAD_FOLDER"] = cl_args.uploads_dir
	
	BIGRAMS = None
	EMBEDDINGS = None
	COVER_TREE = None
	OFFSET = None
	SCALE =None
	
	app.run(host="0.0.0.0", port=8001)

